extern crate proc_macro;
use proc_macro::{TokenStream};
use proc_macro2::Span;
use syn::{ItemTrait, ItemFn, parse_macro_input, Error, token::Comma, Expr, Stmt};
use syn::punctuated::{Punctuated};
use syn::parse::Parser;
use proc_quote::ToTokens;
use crate::err::{TraitObjectGenError, TraitObjectGenErrorType};

mod fn_handler;
mod trait_handler;
mod err;

#[proc_macro_attribute]
pub fn interface(attr: TokenStream, item: TokenStream) -> TokenStream {
    let trait_ : ItemTrait = parse_macro_input!(item as ItemTrait);

    let mut params = Punctuated::<Expr,Comma>::parse_terminated.parse(attr)
        .into_iter()
        .flat_map(|params| params.into_iter());

    let is_root = params.next().map(|expr| {
        if let Expr::Assign(assign) = expr {
            assign.left.to_token_stream().to_string() == "root"
                && assign.right.to_token_stream().to_string() == "true"
        } else {
            false
        }
    }).unwrap_or(false);

    let magic_const = match parse_magic_const(params.next()) {
        Ok(c) => c,
        Err(e) => return Error::from(e).to_compile_error().into()
    };

    if is_root && magic_const.is_none() {
        return Error::from(
            TraitObjectGenError::new(
                Span::call_site(),
                TraitObjectGenErrorType::MissingMagicConstant
            ))
            .to_compile_error()
            .into();
    }

    match trait_handler::gen_trait_obj(&trait_,magic_const) {
        Ok(tokens) => tokens,
        Err(e) => Error::from(e).to_compile_error().into()
    }
}

fn parse_magic_const(expr: Option<Expr>) -> Result<Option<u128>,TraitObjectGenError> {
    let missing_magic_const_err = TraitObjectGenError::new(
        Span::call_site(),
        TraitObjectGenErrorType::MissingMagicConstant
    );

    match expr {
        Some(Expr::Unsafe(expr)) => {
            let stmt = expr.block.stmts.get(0);

            if let Some(Stmt::Expr(Expr::Assign(expr))) = stmt {

                if expr.left.to_token_stream().to_string() == "magic_const" {
                    let const_str = expr.right.to_token_stream().to_string();
                    u128::from_str_radix(&const_str,10)
                        .map(|n| Some(n))
                        .map_err(|_| {
                            TraitObjectGenError::new(
                                Span::call_site(),
                                TraitObjectGenErrorType::MagicConstantParseFailure(const_str)
                            )
                        })
                } else {
                    Err(missing_magic_const_err)
                }

            } else {
                Err(missing_magic_const_err)
            }
        }

        Some(Expr::Assign(_)) => {
            Err(TraitObjectGenError::new(
                Span::call_site(),
                TraitObjectGenErrorType::MagicConstantMissingUnsafe
            ))
        }

        Some(_) => {
            Err(missing_magic_const_err)
        }

        None => Ok(None)
    }
}

#[proc_macro_attribute]
pub fn root(_attr: TokenStream, item: TokenStream) -> TokenStream {
    let fn_: ItemFn = parse_macro_input!(item as ItemFn);

    match fn_handler::gen_entry_point(&fn_) {
        Ok(tokens) => TokenStream::from(tokens),
        Err(e) => Error::from(e).to_compile_error().into(),
    }
}