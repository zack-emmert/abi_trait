use std::{
    iter::FromIterator,
    result::Result,
};
use syn::{punctuated::Punctuated, token::Comma, spanned::Spanned, ItemTrait, TraitItemMethod, ItemFn, FnArg, Pat, ItemImpl, ItemStatic, ItemConst, ImplItemConst, Generics, ItemStruct, ExprStruct, TraitItem, parse_quote, WherePredicate, Type};
use proc_macro::TokenStream;
use proc_macro2::{TokenStream as TStream, Ident};
use proc_quote::{quote, ToTokens};
use quote::format_ident;
use crate::err::{TraitObjectGenError, TraitObjectGenErrorType};

// This function handles generation of a vtable and trait object from a supplied trait definition
pub(crate) fn gen_trait_obj(trait_: &ItemTrait, magic_const: Option<u128>)
    -> Result<TokenStream, TraitObjectGenError> {

    let root = magic_const.is_some();

    if trait_.generics.params.len() > 0 && root {
        return Err(TraitObjectGenError::new(trait_.generics.span(),TraitObjectGenErrorType::GenericRoot))
    }

    assert_static_generics(&trait_.generics)?;

    let trait_ident = &trait_.ident;

    let (trait_fns, vtable_def) = process_functions(trait_)?;

    let TraitFns { externals: extern_functions, trait_impls: impl_methods } = trait_fns;
    let VtableDef { definition: vtable, constructor: vtable_init } = vtable_def;

    // These are all the identifiers computed from the trait name
    let mod_= format_ident!("__abi_trait_{}",trait_ident);
    let vtable_ident = &vtable.ident;
    let vtable_factory = format_ident!("{}Factory",vtable_ident);
    let obj = format_ident!("Dyn{}",trait_ident);

    // RootItems is a simple struct containing all items that only get generated if the trait object
    //  struct is the root
    let root_items = if root {
        RootItems::new(&obj,magic_const.unwrap())?
    } else {
        RootItems::empty()
    };

    let RootItems {
        root_impl,
        load,
        duplicate_err,
        magic_const: hash,
        struct_magic_const: struct_hash
    }  = root_items;

    let generics_idents = &trait_.generics.idents();
    let generics_bounds = &trait_.generics.params;
    let where_ = &trait_.generics.where_clause;

    let expanded = quote!{
        #trait_

        pub use self::#mod_::#obj;
        #duplicate_err

        #[allow(non_snake_case)]
        pub mod #mod_ {
            use super::*;

            mod __internal__ {
                use super::*;
                #hash

                pub(super) extern "C" fn clone_ref <__ABI_TRAIT_RECEIVER__: #trait_ident<#generics_idents> + ::std::clone::Clone,#generics_bounds>
                (src: *const ::std::os::raw::c_void) -> *mut ::std::os::raw::c_void #where_ {
                    let src = src as *const __ABI_TRAIT_RECEIVER__;
                    let out = unsafe {::std::boxed::Box::into_raw(::std::boxed::Box::new((*src).clone()))};
                    out as *mut ::std::os::raw::c_void
                }
                pub(super) extern "C" fn drop_ref <__ABI_TRAIT_RECEIVER__: #trait_ident<#generics_idents>,#generics_bounds>
                (ptr: *mut ::std::os::raw::c_void, recursive: bool) #where_ {
                    unsafe {
                        if recursive {
                            ::std::boxed::Box::from_raw(ptr as *mut __ABI_TRAIT_RECEIVER__);
                        } else if ::std::mem::size_of::<__ABI_TRAIT_RECEIVER__>() != 0 {
                            ::std::alloc::dealloc(ptr as *mut ::std::primitive::u8, ::std::alloc::Layout::new::<__ABI_TRAIT_RECEIVER__>());
                        }

                    }
                }
            }

            #(#extern_functions)*

            struct #vtable_factory <__ABI_TRAIT_RECEIVER__,#generics_bounds>(__ABI_TRAIT_RECEIVER__,#generics_idents);

            impl<__ABI_TRAIT_RECEIVER__: #trait_ident<#generics_idents>,#generics_bounds> #vtable_factory <__ABI_TRAIT_RECEIVER__,#generics_idents> #where_ {
                const NEW: &'static self::#vtable_ident<#generics_idents> = &#vtable_init;
            }

            #vtable

            #[repr(C)]
            pub struct #obj<#generics_bounds> {
                reference: *mut ::std::os::raw::c_void,
                vtable: *const self::#vtable_ident<#generics_idents>,
                ty: ::std::primitive::u64,
                clone_ref: extern "C" fn(src: *const ::std::os::raw::c_void) -> *mut ::std::os::raw::c_void,
                drop_ref: extern "C" fn(ptr: *mut ::std::os::raw::c_void, recursive: bool),
                drop_recursive: ::std::cell::Cell<bool>,
            }

            impl<#generics_bounds> #obj<#generics_idents> #where_ {
                #struct_hash
                pub fn new<__ABI_TRAIT_RECEIVER__: #trait_ident<#generics_idents> + ::std::clone::Clone + ::std::marker::Send + ::std::marker::Sync + 'static>(
                    val: __ABI_TRAIT_RECEIVER__,
                ) -> Self {
                    fn _obj_safe<#generics_idents>(_: &dyn #trait_ident<#generics_idents>) {}

                    let mut hasher = ::std::collections::hash_map::DefaultHasher::new();
                    ::std::hash::Hash::hash(&::std::any::TypeId::of::<__ABI_TRAIT_RECEIVER__>(),&mut hasher);

                    Self {
                        reference: ::std::boxed::Box::into_raw(::std::boxed::Box::new(val)) as *mut ::std::os::raw::c_void,
                        vtable: self:: #vtable_factory ::<__ABI_TRAIT_RECEIVER__,#generics_idents>::NEW,
                        ty: ::std::hash::Hasher::finish(&hasher),
                        clone_ref: self::__internal__::clone_ref::<__ABI_TRAIT_RECEIVER__,#generics_idents>,
                        drop_ref: self::__internal__::drop_ref::<__ABI_TRAIT_RECEIVER__,#generics_idents>,
                        drop_recursive: ::std::cell::Cell::new(true),
                    }
                }
                #load
                pub fn downcast_ref<__ABI_TRAIT__DOWNCAST__: #trait_ident<#generics_idents> + 'static>(&self) -> ::std::option::Option<&__ABI_TRAIT__DOWNCAST__> {
                    let mut hasher = ::std::collections::hash_map::DefaultHasher::new();
                    ::std::hash::Hash::hash(&::std::any::TypeId::of::<__ABI_TRAIT__DOWNCAST__>(),&mut hasher);

                    if ::std::hash::Hasher::finish(&hasher) == self.ty {
                        unsafe { ::std::option::Option::Some(&*(self.reference as *const __ABI_TRAIT__DOWNCAST__)) }
                    }
                    else {
                        ::std::option::Option::None
                    }
                }

                pub fn downcast_mut<__ABI_TRAIT__DOWNCAST__: #trait_ident<#generics_idents> + 'static>(&self) -> ::std::option::Option<&mut __ABI_TRAIT__DOWNCAST__> {
                    let mut hasher = ::std::collections::hash_map::DefaultHasher::new();
                    ::std::hash::Hash::hash(&::std::any::TypeId::of::<__ABI_TRAIT__DOWNCAST__>(),&mut hasher);

                    if ::std::hash::Hasher::finish(&hasher) == self.ty {
                        unsafe { ::std::option::Option::Some(&mut *(self.reference as *mut __ABI_TRAIT__DOWNCAST__)) }
                    }
                    else {
                        ::std::option::Option::None
                    }
                }
            }

            impl<#generics_bounds> #trait_ident<#generics_idents> for self::#obj<#generics_idents> #where_ {
                #(#impl_methods)*
            }

            impl<#generics_bounds> ::std::clone::Clone for self::#obj<#generics_idents> {
                fn clone(&self) -> Self {
                    Self {
                        reference: (self.clone_ref)(self.reference as *const ::std::os::raw::c_void),
                        vtable: self.vtable,
                        ty: self.ty,
                        clone_ref: self.clone_ref,
                        drop_ref: self.drop_ref,
                        drop_recursive: ::std::clone::Clone::clone(&self.drop_recursive),
                    }
                }
            }
            impl<#generics_bounds> ::std::ops::Drop for self::#obj<#generics_idents> {
                fn drop(&mut self) {
                    (self.drop_ref)(self.reference,self.drop_recursive.get());
                }
            }

            #root_impl

            unsafe impl<#generics_bounds> ::std::marker::Send for self::#obj<#generics_idents> {}
            unsafe impl<#generics_bounds> ::std::marker::Sync for self::#obj<#generics_idents> {}
            unsafe impl<#generics_bounds> ::abi_trait::FFISafe for self::#obj<#generics_idents> {}
            impl<#generics_bounds> ::std::panic::UnwindSafe for self::#obj<#generics_idents> {}
            impl<#generics_bounds> ::std::panic::RefUnwindSafe for self::#obj<#generics_idents> {}
        }
    };
    Ok(TokenStream::from(expanded))
}

fn gen_extern_fn(method: &TraitItemMethod, trait_: &ItemTrait) -> Result<ItemFn,TraitObjectGenError> {

    let trait_ident = &trait_.ident;

    let name = &method.sig.ident;
    let ret = &method.sig.output;

    let generics_bounds = &trait_.generics.params;
    let generics_idents = &trait_.generics.idents();
    let where_ = &trait_.generics.where_clause;

    let (ptr_type, read, deref) = match &method.sig.receiver() {
        Some(FnArg::Receiver(ref r)) => {
            let ptr_type = if r.mutability.is_some() {
                quote!(*mut)
            } else {
                quote!(*const)
            };

            let (read, deref) = if r.reference.is_none() {
                (Some(quote!(.read())),None)
            } else {
                (None,Some(quote!(*)))
            };

            (ptr_type,read,deref)
        }
        _ => return Err(TraitObjectGenError::new(
            method.sig.span(),
            TraitObjectGenErrorType::NoReceiverError(format!("{}",method.sig.to_token_stream()))
        ))
    };

    // The skip is to get rid of the receiver
    let args = method.sig.inputs.iter().skip(1);

    let call_args = arg_idents(&method.sig.inputs)?;

    return Ok(parse_quote! {
        pub extern "C" fn #name <__ABI_TRAIT_RECEIVER__: #trait_ident<#generics_idents>,#generics_bounds> (self_: #ptr_type ::std::os::raw::c_void, #(#args),*) #ret #where_ {
            unsafe {
                ::std::panic::catch_unwind(::std::panic::AssertUnwindSafe(|| {
                    (#deref(self_ as #ptr_type __ABI_TRAIT_RECEIVER__)) #read . #name (#call_args)
                })).map_err(::std::panic::resume_unwind).unwrap()
            }
        }
    });
}

fn arg_idents(args: &Punctuated<FnArg, Comma>)
    -> Result<Punctuated<Box<Pat>, Comma>, TraitObjectGenError> {
    Result::from_iter(
        args.iter()
            .skip(1)
            .map(|arg| {
                match arg {
                    FnArg::Typed(captured) => Ok(captured.pat.clone()),
                    FnArg::Receiver(r) =>
                        Err(TraitObjectGenError::new(
                            r.self_token.span,
                            TraitObjectGenErrorType::ReceiverArgsNotSupported))
                }})
    )
}

struct RootItems {
    root_impl: Option<ItemImpl>,
    load: Option<ItemFn>,
    duplicate_err: Option<ItemStatic>,
    magic_const: Option<ItemConst>,
    struct_magic_const: Option<ImplItemConst>,
}

impl RootItems {
    fn new(obj: &syn::Ident, magic_const: u128) -> Result<RootItems,std::io::Error> {
        Ok(Self {
            root_impl: Some(parse_quote! {unsafe impl ::abi_trait::Root for #obj {} }),
            load: Some(parse_quote! {
                pub fn load(lib: &'static ::abi_trait::libloading::Library) ->
                    ::std::result::Result<#obj, ::abi_trait::err::LoadError> {
                    ::abi_trait::load_root_module::<#obj>(lib,self::__internal__::ABI_TRAIT_MAGIC_CONST)
                }
            }),
            // duplicate_err throws a compiler error if multiple root interfaces are defined
            duplicate_err: Some(parse_quote!{
                #[no_mangle]
                pub static ABI_TRAIT_ROOT_MODULE: u64 = 0;
            }),
            magic_const: Some(parse_quote!(pub(super) const ABI_TRAIT_MAGIC_CONST: ::std::primitive::u128 = #magic_const ;)),
            struct_magic_const: Some(parse_quote!(pub const ABI_TRAIT_MAGIC_CONST: ::std::primitive::u128 = self::__internal__::ABI_TRAIT_MAGIC_CONST;)),
        })
    }

    fn empty() -> Self {
        Self {
            root_impl: None,
            load: None,
            duplicate_err: None,
            magic_const: None,
            struct_magic_const: None,
        }
    }
}

trait GenericsExt {
    fn idents(&self) -> Punctuated<&Ident,Comma>;
}

impl GenericsExt for Generics {
    fn idents(&self) -> Punctuated<&Ident, Comma> {
        self.type_params().map(|ty| &ty.ident).collect()
    }
}

fn assert_static_generics(g: &Generics) -> Result<(),TraitObjectGenError> {
    use syn::TypeParamBound;
    let mut needs_where_check = Vec::new();
    for ty in g.type_params() {
        if ty.bounds.iter().filter_map(|b| {
            if let TypeParamBound::Lifetime(lt) = b {
                Some(lt)
            } else { None }
        }).find(|lt| {
            &lt.ident.to_string() == "static"
        }).is_none() {
            // If the static bound is not in the generics bounds, the next place to look is the where clause
            needs_where_check.push(ty);
        }
    }

    for ty in needs_where_check {
        // If there's no where clause, error out with the first non-static generic
        let where_clause = g.where_clause.as_ref().ok_or_else(|| {
            TraitObjectGenError::new(ty.span(),TraitObjectGenErrorType::NonStaticGeneric(ty.ident.to_string()))
        })?;

        let has_static_bound = where_clause.predicates.iter().find_map(|p| {
            // Linear search through where clause predicates to find the correct type ident
            match p {
                WherePredicate::Type(pred) => {
                    match pred.bounded_ty {
                        Type::Path(ref path) => {
                            if path.path.to_token_stream().to_string() == ty.ident.to_string() {
                                Some(&pred.bounds)
                            } else {
                                None
                            }
                        },
                        _ => None,
                    }

                },
                _ => None
            }
        }).map(|bounds| {
            // Search through the bounds to look for 'static
            bounds.iter().any(|bound| {
                match bound {
                    TypeParamBound::Trait(_) => { false }
                    TypeParamBound::Lifetime(lt) => {
                        &lt.ident.to_string() == "static"
                    }
                }
            })
        }).ok_or_else(|| {
            // If the type isn't in the where clause, error out
            TraitObjectGenError::new(ty.span(),TraitObjectGenErrorType::NonStaticGeneric(ty.ident.to_string()))
        })?;

        if !has_static_bound {
            return Err(TraitObjectGenError::new(ty.span(),TraitObjectGenErrorType::NonStaticGeneric(ty.ident.to_string())));
        }
    }

    Ok(())
}

struct TraitFns {
    externals: Vec<ItemFn>,
    trait_impls: Vec<ItemFn>,
}

struct VtableDef {
    definition: ItemStruct,
    constructor: ExprStruct,
}

fn process_functions(trait_: &ItemTrait) -> Result<(TraitFns, VtableDef),TraitObjectGenError> {

    let generics_idents = &trait_.generics.idents();
    let generics_bounds = &trait_.generics.params;

    let item_count = trait_.items.len();
    //List of all extern functions stored in the vtable. Each one corresponds to a trait impl method
    let mut externals = Vec::with_capacity(item_count);
    // Type signatures of all extern methods, used in the definition of the vtable struct
    let mut vtable_members = Vec::with_capacity(item_count);
    // function references to each of the extern methods, used to initialize the vtable
    let mut vtable_field_inits = Vec::with_capacity(item_count);
    // implementations of each of the trait methods that delegate to the extern functions
    let mut trait_impls = Vec::with_capacity(item_count);

    for item in &trait_.items {
        if let TraitItem::Method(method) = item {

            // Separate out name, return type, and arguments of each trait method
            let ident = &method.sig.ident;
            let ret = &method.sig.output;
            let args = &method.sig.inputs;

            // call_idents corresponds to the names of all function arguments, separated by commas.
            // This series is used to call the underlying trait method on the reference type.
            let call_idents = arg_idents(&args)?;

            // The first argument has to be the receiver, and if it's not then there isn't one.
            // (the compiler already catches it if the receiver is present, but not the fist arg)
            let first_arg = args.first();
            let ptr_cast = if let Some(FnArg::Receiver(self_)) = first_arg {
                if self_.mutability.is_some() {
                    None
                } else {
                    Some(quote!(as *const ::std::os::raw::c_void))
                }
            } else {
                return Err(TraitObjectGenError::new(
                    ident.span(),
                    TraitObjectGenErrorType::NoReceiverError(ident.to_string())
                ));
            };

            // This code generates the extern function from the trait method, and copies the
            // arguments for later
            let extern_fn = gen_extern_fn(&method, trait_)?;
            let extern_args = extern_fn.sig.inputs.clone();
            externals.push(extern_fn);

            // The preamble has a couple elements:
            //  1. If the method receiver is by value, set the `drop_recursive` flag to false to
            //     avoid a double-free
            //  2. Call abi_trait::check for each of the argument types and the return type in order
            //     to throw a compiler error if one of the types is not FFI Safe
            let preamble: Vec<Option<TStream>> = args.iter().map(|arg| {
                // Process all of the arguments
                match arg {
                    FnArg::Typed(arg) => {
                        let ty = &arg.ty;
                        Some(quote!(::abi_trait::__check__::<#ty>()))
                    }
                    FnArg::Receiver(r) => {
                        if r.reference.is_none() {
                            Some(quote!(self.drop_recursive.set(false)))
                        } else { None }
                    }
                }
            }).chain({
                // And process the return type if there is one
                if let syn::ReturnType::Type(_, ty) = ret {
                    Some(Some(parse_quote!(::abi_trait::__check__::<#ty>())))
                } else { None }
            }).collect();

            trait_impls.push(parse_quote!{
                fn #ident (#args) # ret {
                    #(#preamble);*;
                    unsafe { ((*self.vtable).#ident)(self.reference #ptr_cast, #call_idents) }
                }
            });
            vtable_members.push(quote!{
                #ident : extern "C" fn (#extern_args) #ret
            });
            vtable_field_inits.push(quote!{
                #ident : self::#ident::<__ABI_TRAIT_RECEIVER__,#generics_idents>
            });

        }
    }

    let vtable_ident = format_ident!("{}VTable",&trait_.ident);

    let vtable = parse_quote! {
        #[repr(C)]
        struct #vtable_ident<#generics_bounds> {
            #(#vtable_members),*
        }
    };

    let vtable_init = parse_quote!{
        self::#vtable_ident {
            #(#vtable_field_inits),*
        }
    };

    Ok((TraitFns {externals, trait_impls}, VtableDef {definition: vtable, constructor: vtable_init }))
}