use syn::{spanned::Spanned, ItemFn,ReturnType};
use proc_macro::TokenStream;
use proc_quote::quote;
use crate::err::{RootLoaderGenError,RootLoaderGenErrorType};

// This module produces the function that provides an entry point into the library from the binary
pub(crate) fn gen_entry_point(fn_ : &ItemFn) -> Result<TokenStream,RootLoaderGenError> {

    let ident = &fn_.sig.ident;

    let obj_path = match &fn_.sig.output {
        ReturnType::Default => {
            return Err(RootLoaderGenError::new(
                fn_.sig.output.span(),
                RootLoaderGenErrorType::MissingReturnType,
            ));}
        ReturnType::Type(_, ty) => { ty }
    };

    let expanded = quote! {
        #fn_

        #[no_mangle]
        pub static ABI_TRAIT_API_VER: ::std::primitive::u64 = 0;
        #[no_mangle]
        pub static ABI_TRAIT_MAGIC_CONST: ::std::primitive::u128 = #obj_path :: ABI_TRAIT_MAGIC_CONST;
        #[no_mangle]
        pub extern "C" fn __abi_trait_get_root_module(obj: *mut #obj_path) {

            fn _check<O: ::abi_trait::Root, F: ::std::ops::Fn() -> O>(f: F) {}

            _check(#ident);
            unsafe { obj.write(#ident()); }
        }
    };
    Ok(TokenStream::from(expanded))
}