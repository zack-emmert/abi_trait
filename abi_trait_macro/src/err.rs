use proc_macro2::Span;
use std::fmt::{self,Display,Formatter};
use std::error::Error;

#[derive(Debug)]
pub(crate) struct TraitObjectGenError {
    err_type: TraitObjectGenErrorType,
    span: Span
}

impl TraitObjectGenError {
    pub fn new(span: Span, err_type: TraitObjectGenErrorType) -> Self {
        Self {
            err_type,
            span
        }
    }
}

impl Display for TraitObjectGenError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        use self::TraitObjectGenErrorType::*;
        match &self.err_type {
            NoReceiverError(s) => write!(f,"Method `{}` has no receiver.",s),
            ReceiverArgsNotSupported => write!(f, "Receiver arguments not supported here."),
            GenericRoot => write!(f,"Root interface cannot support generic types."),
            NonStaticGeneric(s) => write!(f,"Lifetime bound not satisfied for type `{}`: 'static.",s),
            Io(e) => e.fmt(f),
            MissingMagicConstant => write!(f,"Magic constant missing from root definition."),
            MagicConstantMissingUnsafe => write!(f,"Magic constant must be declared in `unsafe` block."),
            MagicConstantParseFailure(s) => write!(f, "Unable to parse magic constant: {}",s),
        }
    }
}

impl Error for TraitObjectGenError {}

impl From<TraitObjectGenError> for syn::Error {
    fn from(e: TraitObjectGenError) -> Self {
        syn::Error::new(e.span,e)
    }
}

impl From<std::io::Error> for TraitObjectGenError {
    fn from(e: std::io::Error) -> Self {
        Self {
            err_type: TraitObjectGenErrorType::Io(e),
            span: Span::call_site(),
        }
    }
}

#[derive(Debug)]
pub(crate) enum TraitObjectGenErrorType {
    NoReceiverError(String),
    ReceiverArgsNotSupported,
    NonStaticGeneric(String),
    GenericRoot,
    Io(std::io::Error),
    MissingMagicConstant,
    MagicConstantMissingUnsafe,
    MagicConstantParseFailure(String),
}

#[derive(Debug,Clone)]
pub(crate) struct RootLoaderGenError {
    err_type: RootLoaderGenErrorType,
    span: Span
}

impl RootLoaderGenError {
    pub fn new(span: Span, err_type: RootLoaderGenErrorType) -> Self {
        Self {
            err_type,
            span
        }
    }
}

impl Display for RootLoaderGenError {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        use self::RootLoaderGenErrorType::*;
        match &self.err_type {
            MissingReturnType => write!(f,"#[root_module] declared on a function returning ()."),
        }
    }
}

impl Error for RootLoaderGenError {}

impl From<RootLoaderGenError> for syn::Error {
    fn from(e: RootLoaderGenError) -> Self {
        syn::Error::new(e.span,e)
    }
}

#[derive(Debug,Clone)]
pub(crate) enum RootLoaderGenErrorType {
    MissingReturnType,
}

