## How it works ##

### Trait Object Generator ###

The code in the `#[interface]` macro generates a Virtual Table and a struct akin to a homegrown trait object, along
with some internal helpers. 
So given this trait:

```rust
trait Trait {
    fn foo(&self);
    fn bar(&mut self);
    fn baz(self) -> i32;
    fn qux(&self, x: u16);
    fn quux(&self, y: &mut u16);
}
```

the macro generates code that looks like this:

```rust
pub use self::__abi_trait_Trait::DynTrait;
#[allow(non_snake_case)]
pub mod __abi_trait_Trait {
    use super::*;
    mod __internal__ {
        use super::*;
        pub(super) extern "C" fn clone_ref<__ABI_TRAIT_RECEIVER__: Trait + ::std::clone::Clone>(
            src: *const ::std::os::raw::c_void,
        ) -> *mut ::std::os::raw::c_void {
            let src = src as *const __ABI_TRAIT_RECEIVER__;
            let out =
                unsafe { ::std::boxed::Box::into_raw(::std::boxed::Box::new((*src).clone())) };
            out as *mut ::std::os::raw::c_void
        }
        pub(super) extern "C" fn drop_ref<__ABI_TRAIT_RECEIVER__: Trait>(
            ptr: *mut ::std::os::raw::c_void,
            recursive: bool,
        ) {
            unsafe {
                if recursive {
                    ::std::boxed::Box::from_raw(ptr as *mut __ABI_TRAIT_RECEIVER__);
                } else if ::std::mem::size_of::<__ABI_TRAIT_RECEIVER__>() != 0 {
                    ::std::alloc::dealloc(
                        ptr as *mut ::std::primitive::u8,
                        ::std::alloc::Layout::new::<__ABI_TRAIT_RECEIVER__>(),
                    );
                }
            }
        }
    }
    pub extern "C" fn foo<__ABI_TRAIT_RECEIVER__: Trait>(self_: *const ::std::os::raw::c_void) {
        unsafe {
            ::std::panic::catch_unwind(::std::panic::AssertUnwindSafe(|| {
                (*(self_ as *const __ABI_TRAIT_RECEIVER__)).foo()
            }))
                .map_err(::std::panic::resume_unwind)
                .unwrap()
        }
    }
    pub extern "C" fn bar<__ABI_TRAIT_RECEIVER__: Trait>(self_: *mut ::std::os::raw::c_void) {
        unsafe {
            ::std::panic::catch_unwind(::std::panic::AssertUnwindSafe(|| {
                (*(self_ as *mut __ABI_TRAIT_RECEIVER__)).bar()
            }))
                .map_err(::std::panic::resume_unwind)
                .unwrap()
        }
    }
    pub extern "C" fn baz<__ABI_TRAIT_RECEIVER__: Trait>(
        self_: *const ::std::os::raw::c_void,
    ) -> i32 {
        unsafe {
            ::std::panic::catch_unwind(::std::panic::AssertUnwindSafe(|| {
                (self_ as *const __ABI_TRAIT_RECEIVER__).read().baz()
            }))
                .map_err(::std::panic::resume_unwind)
                .unwrap()
        }
    }
    pub extern "C" fn qux<__ABI_TRAIT_RECEIVER__: Trait>(
        self_: *const ::std::os::raw::c_void,
        x: u16,
    ) {
        unsafe {
            ::std::panic::catch_unwind(::std::panic::AssertUnwindSafe(|| {
                (*(self_ as *const __ABI_TRAIT_RECEIVER__)).qux(x)
            }))
                .map_err(::std::panic::resume_unwind)
                .unwrap()
        }
    }
    pub extern "C" fn quux<__ABI_TRAIT_RECEIVER__: Trait>(
        self_: *const ::std::os::raw::c_void,
        y: &mut u16,
    ) {
        unsafe {
            ::std::panic::catch_unwind(::std::panic::AssertUnwindSafe(|| {
                (*(self_ as *const __ABI_TRAIT_RECEIVER__)).quux(y)
            }))
                .map_err(::std::panic::resume_unwind)
                .unwrap()
        }
    }
    struct TraitVTableFactory<__ABI_TRAIT_RECEIVER__>(__ABI_TRAIT_RECEIVER__);
    impl<__ABI_TRAIT_RECEIVER__: Trait> TraitVTableFactory<__ABI_TRAIT_RECEIVER__> {
        const NEW: &'static self::TraitVTable = &self::TraitVTable {
            foo: self::foo::<__ABI_TRAIT_RECEIVER__>,
            bar: self::bar::<__ABI_TRAIT_RECEIVER__>,
            baz: self::baz::<__ABI_TRAIT_RECEIVER__>,
            qux: self::qux::<__ABI_TRAIT_RECEIVER__>,
            quux: self::quux::<__ABI_TRAIT_RECEIVER__>,
        };
    }
    #[repr(C)]
    struct TraitVTable {
        foo: extern "C" fn(self_: *const ::std::os::raw::c_void),
        bar: extern "C" fn(self_: *mut ::std::os::raw::c_void),
        baz: extern "C" fn(self_: *const ::std::os::raw::c_void) -> i32,
        qux: extern "C" fn(self_: *const ::std::os::raw::c_void, x: u16),
        quux: extern "C" fn(self_: *const ::std::os::raw::c_void, y: &mut u16),
    }
    #[repr(C)]
    pub struct DynTrait {
        reference: *mut ::std::os::raw::c_void,
        vtable: *const self::TraitVTable,
        ty: ::std::primitive::u64,
        clone_ref: extern "C" fn(src: *const ::std::os::raw::c_void) -> *mut ::std::os::raw::c_void,
        drop_ref: extern "C" fn(ptr: *mut ::std::os::raw::c_void, recursive: bool),
        drop_recursive: ::std::cell::Cell<bool>,
    }
    impl DynTrait {
        pub fn new<
            __ABI_TRAIT_RECEIVER__: Trait + ::std::clone::Clone + ::std::marker::Send + ::std::marker::Sync + 'static,
        >(
            val: __ABI_TRAIT_RECEIVER__,
        ) -> Self {
            fn _obj_safe(_: &dyn Trait) {}
            let mut hasher = ::std::collections::hash_map::DefaultHasher::new();
            ::std::hash::Hash::hash(
                &::std::any::TypeId::of::<__ABI_TRAIT_RECEIVER__>(),
                &mut hasher,
            );
            Self {
                reference: ::std::boxed::Box::into_raw(::std::boxed::Box::new(val))
                    as *mut ::std::os::raw::c_void,
                vtable: self::TraitVTableFactory::<__ABI_TRAIT_RECEIVER__>::NEW,
                ty: ::std::hash::Hasher::finish(&hasher),
                clone_ref: self::__internal__::clone_ref::<__ABI_TRAIT_RECEIVER__>,
                drop_ref: self::__internal__::drop_ref::<__ABI_TRAIT_RECEIVER__>,
                drop_recursive: ::std::cell::Cell::new(true),
            }
        }
        pub fn downcast_ref<__ABI_TRAIT__DOWNCAST__: Trait + 'static>(
            &self,
        ) -> ::std::option::Option<&__ABI_TRAIT__DOWNCAST__> {
            let mut hasher = ::std::collections::hash_map::DefaultHasher::new();
            ::std::hash::Hash::hash(
                &::std::any::TypeId::of::<__ABI_TRAIT__DOWNCAST__>(),
                &mut hasher,
            );
            if ::std::hash::Hasher::finish(&hasher) == self.ty {
                unsafe {
                    ::std::option::Option::Some(
                        &*(self.reference as *const __ABI_TRAIT__DOWNCAST__),
                    )
                }
            } else {
                ::std::option::Option::None
            }
        }
        pub fn downcast_mut<__ABI_TRAIT__DOWNCAST__: Trait + 'static>(
            &self,
        ) -> ::std::option::Option<&mut __ABI_TRAIT__DOWNCAST__> {
            let mut hasher = ::std::collections::hash_map::DefaultHasher::new();
            ::std::hash::Hash::hash(
                &::std::any::TypeId::of::<__ABI_TRAIT__DOWNCAST__>(),
                &mut hasher,
            );
            if ::std::hash::Hasher::finish(&hasher) == self.ty {
                unsafe {
                    ::std::option::Option::Some(
                        &mut *(self.reference as *mut __ABI_TRAIT__DOWNCAST__),
                    )
                }
            } else {
                ::std::option::Option::None
            }
        }
    }
    impl Trait for self::DynTrait {
        fn foo(&self) {
            unsafe { ((*self.vtable).foo)(self.reference as *const ::std::os::raw::c_void) }
        }
        fn bar(&mut self) {
            unsafe { ((*self.vtable).bar)(self.reference) }
        }
        fn baz(self) -> i32 {
            self.drop_recursive.set(false);
            ::abi_trait::__check__::<i32>();
            unsafe { ((*self.vtable).baz)(self.reference as *const ::std::os::raw::c_void) }
        }
        fn qux(&self, x: u16) {
            ::abi_trait::__check__::<u16>();
            unsafe { ((*self.vtable).qux)(self.reference as *const ::std::os::raw::c_void, x) }
        }
        fn quux(&self, y: &mut u16) {
            ::abi_trait::__check__::<&mut u16>();
            unsafe { ((*self.vtable).quux)(self.reference as *const ::std::os::raw::c_void, y) }
        }
    }
    impl ::std::clone::Clone for self::DynTrait {
        fn clone(&self) -> Self {
            Self {
                reference: (self.clone_ref)(self.reference as *const ::std::os::raw::c_void),
                vtable: self.vtable,
                ty: self.ty,
                clone_ref: self.clone_ref,
                drop_ref: self.drop_ref,
                drop_recursive: ::std::clone::Clone::clone(&self.drop_recursive),
            }
        }
    }
    impl ::std::ops::Drop for self::DynTrait {
        fn drop(&mut self) {
            (self.drop_ref)(self.reference, self.drop_recursive.get());
        }
    }
    unsafe impl ::std::marker::Send for self::DynTrait {}
    unsafe impl ::std::marker::Sync for self::DynTrait {}
    unsafe impl ::abi_trait::FFISafe for self::DynTrait {}
    impl ::std::panic::UnwindSafe for self::DynTrait {}
    impl ::std::panic::RefUnwindSafe for self::DynTrait {}
}
```

Generated code is placed in a new module which imports the wrapped trait. FFI methods (`extern`)
methods are declared that are generic over types that implement the wrapped trait.

These `extern` methods rely on `catch_unwind` and `resume_unwind` to ensure that a panic triggered within a plugin does
not cause UB by panicking over the FFI boundary. Aborting panics are not caught by `catch_unwind`, however, but because
these do not cause an unwinding of the stack, they don't create UB in this case.

This is almost identical to how dynamic dispatch works "under the hood." This does not seek to replace dynamic dispatch,
just provide functionality across library boundaries that are compiled with different compiler versions (e.g., plugins
and application extensions distributed as shared libraries).

The calls to `abi_trait::__check__` are used to throw a compiler error in the case that the return type
or one of the arguments cannot be safely passed over the FFI boundary.

Of particular note is the seemingly redundant `InterfaceVTableFactory` struct. Its purpose is to trick the compiler
into generating virtual table instances as compile-time constants. It stores a value of the receiver type solely to
prevent the compiler from complaining about the generic type not being used, and the real work is done by the
associated constant `NEW`. As the `InterfaceVTableFactory` is monomorphized, it generates corresponding constants for
each concrete type, which makes `NEW` act essentially as a compile-time constructor for instances of `InterfaceVTable`.

The `drop_recursive` attribute is used to avoid a double-free if one of the trait methods takes `self` by value.
If this happens, the underlying method calls the `Drop` implementation of the stored reference. If the `Drop`
implementation of the pseudo-trait object itself also called its `Drop` implementation, this would result in a
double-free of any allocations owned by the stored reference, because a **bitwise** copy is given to the method. 
Instead, if the underlying reference is taken by value, the reference pointer is only **non-recursively** deallocated,
preventing a double-free of any such allocations.

The `downcast` functions are implemented by calculating a hash of the `TypeId` of the downcasted type, and comparing it
to the `TypeId` hash of the reference's actual type, returning `Some` if they match. Because `TypeId`s are only 
consistent within a compiled object, this single check simultaneously ensures that the type is correct, and that the type
can be safely downcasted without encountering ABI stability problems.

Additionally, if `root` and a magic constant are passed as parameters to the interface macro, some additional code will 
be generated:

```rust
#[no_mangle]
pub static ABI_TRAIT_ROOT_MODULE: u8 = 0;
#[allow(non_snake_case)]
pub mod __abi_trait_Trait {
// snip
```
This global simply triggers a compiler error if one attempts to create multiple root interfaces.

```rust
mod __internal__ {
    const ABI_TRAIT_MAGIC_CONST: u128 = /* ... */;
// snip
```

The magic constant ensures that the binary and any shared libraries it loads all depend on the
**exact same** interface crate. The `interface` handles generating this hash.

```rust
pub fn new<__ABI_TRAIT_RECEIVER__: Interface + Clone + Send + Sync>(val: T) {
    // ...
}
pub fn load(lib: &'static ::abi_trait::libloading::Library) -> Result<self::DynTrait, ::abi_trait::err::LoadError> {
    ::abi_trait::load_root_module::<self::DynTrait>(lib, self::__internal__::ABI_TRAIT_MAGIC_CONST)
}
```
The `load` function is specific to the root object and allows for it to be loaded directly from the shared library.

```rust
impl Drop for DynTrait { 
// snip
}

unsafe impl ::abi_trait::Root for DynTrait {}
```

The `Root` marker trait is used to indicate which pseudo-Trait Object is designated as the root of the plugin, and
is instrumental to error handling done by code generated by the `#[root]` macro below.

### Root Module Loader ###

Given the below:

```rust
#[derive(Clone)]
pub struct Plugin(u16);

impl Trait for Plugin {
    fn foo(&self) {println!("{}",self.0)}
    fn bar(&mut self) {self.0+=1}
    fn baz(self) -> u16 {self.0}
    fn qux(&mut self, x: u16) {self.0 = x}
}

#[root]
fn load() -> DynTrait {
    DynTrait::new(Plugin(0))
}
```

the `#[root]` macro generates this:

```rust
#[no_mangle]
pub static ABI_TRAIT_API_VER: ::std::primitive::u64 = 0;
#[no_mangle]
pub static ABI_TRAIT_MAGIC_CONST: ::std::primitive::u128 = DynTrait::ABI_TRAIT_MAGIC_CONST;
#[no_mangle]
pub extern "C" fn __abi_trait_get_root_module(obj: *mut DynTrait) {

    fn _check<O: ::abi_trait::Root, F: ::std::ops::Fn() -> O>(f: F) {}

    _check(load);
    unsafe { obj.write(load()); }
}
```

Firstly, the `ABI_TRAIT_API_VER` is used as a sanity check, ensuring the library was produced
by a crate utilizing `abi_trait` that has the same API version, and helps prevent loading invalid or
corrupted libraries.

The value of `ABI_TRAIT_MAGIC_CONST` is the `magic_const` parameter passed to the root object, and it ensures the binary
and the plugin both depend on the **exact same** interface crate.

Second, the `__abi_trait_get_root_module` function provides an entry point used to load the root module.
The check function is responsible for ensuring that the macro was declared on a function taking no parameters, 
and returning a type that implements `abi_trait::Root`, a marker trait used by the `#[interface]` macro to 
indicate the root object. This way, an invalid type can't be loaded as root, and any attempts to do so will 
result in a compiler error.

`obj` is a pointer to an uninitialized instance of the trait object struct in question.