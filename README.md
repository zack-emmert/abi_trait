# abi_trait #

abi_trait provides macros for defining trait objects with compatible ABIs across compiler versions.

In the short term this allows for defining interfaces for application 
extensions distributed as shared libraries. Longer term, supporting more
complex data types (and maybe associated types?) should be supported.

Contributions are always welcome.

## Usage ## 

`Cargo.toml`:
```toml
[dependencies]
abi_trait = { git = "https://gitlab.com/zack-emmert/abi_trait.git"}
```

Add the `#[interface]` attribute to a trait you want to create an FFI-safe dynamic dispatch object for:
```rust
use abi_trait::interface;

#[interface]
trait Trait {
    fn foo(&self);
    fn bar(&mut self);
    fn baz(self) -> i32;
    fn qux(&self, x: u16);
    fn quux(&self, y: &mut u16);
}
```
If you would like a given trait to represent the root object of downstream plugins, simply pass `root` as a parameter 
to `#[interface]`, along with a magic constant used to ensure that both the plugin and binary are using the same version
of the interface.

###**THIS MAGIC CONSTANT MUST BE UPDATED EVERY TIME CHANGES TO THE INTERFACE CRATE ARE PUBLISHED. FAILURE TO DO SO WILL CAUSE UNDEFINED BEHAVIOR.**

Internally, the magic constant is stored as a `u128`, so there is plenty of room to expand.

```rust
use abi_trait::interface;
#[interface(root, unsafe { magic_const = 0 })]
trait Trait {
    fn foo(&self);
    fn bar(&mut self);
    fn baz(self) -> i32;
    fn qux(&self, x: u16);
    fn quux(&self, y: &mut u16);
}
```

The root object represents the pseudo-Trait Object that is loaded directly from the compiled plugin from the binary. 
Any other objects are loaded by calling functions defined on the root.

In downstream plugins you can add the`#[root]` attribute on a function that returns the pseudo-Trait Object and takes no
parameters. This function is called as part of the loading process for the root object.

```rust
use abi_trait::root;
use interface_crate::{DynTrait,Trait};
struct Struct(u16);

impl Trait for Struct {
    fn foo(&self) {/* ... */}
    fn bar(&mut self) {/* ... */}
    fn baz(self) -> i32 {/* ... */}
    fn qux(&self, x: u16) {/* ... */}
    fn quux(&self, y: &mut u16) {/* ... */}
}

#[root]
fn load() -> DynTrait {
    DynTrait::new(Struct(0))
}
```
Additionally, the trait objects are equipped with the `downcast_ref` and `downcast_mut` methods, which attempt to
downcast the trait objects to their original type, returning an `Option<&T>` and `Option<&mut T>`, respectively.
These return `Some` if the type downcasted to is the same as the type it was created with, and the function is called
within the same binary or shared library whose code originally created the pseudo-trait object. If either of these
conditions are not met, the functions will return `None`.

See `INTERNALS.md` for the implementation details of these two functions.

Lastly, In order to properly load a plugin and execute functions defined within it, do as follows:

```rust
use interface_crate::{DynTrait,Trait};

fn main() -> Result<(),Box<dyn std::error::Error>> {
    
    // Note that this is a convenience function that works by explicitly leaking memory 
    //  and that any other method of acquiring an &'static Library will serve the same purpose
    let lib = abi_trait::load("/path/to/plugin.so")?;
    
    let mut obj = DynTrait::load(lib)?;
    
    obj.foo();
    obj.bar();
    println!("{}",obj.baz());
    obj.qux(2);
    /* etc... */
    Ok(())
}
```

**NOTE:** Generally speaking, identifiers, type parameters, and other items beginning with `__abi_trait_` 
(case *in*sensitive) are reserved for internal use by the macros.

## Roadmap ## 

- [x] Documentation/examples
- [x] Error messaging/failures for invalid traits
- [x] Support for generic traits
- [ ] Support for associated types
- [x] Simpler, safer API for binaries that load the traits