#![deny(clippy::style)]
#![deny(clippy::complexity)]
#![warn(clippy::perf)]
#![deny(rust_2018_idioms)]
#![warn(clippy::pedantic)]
#![warn(clippy::cognitive_complexity)]

/*!
# abi_trait #

abi_trait provides macros for defining trait objects with compatible ABIs across compiler versions.

In the short term this allows for defining interfaces for application
extensions distributed as shared libraries. Longer term, supporting more
complex data types (and maybe associated types?) should be supported.

Contributions are always welcome.

## Usage ##

`Cargo.toml`:
```toml
[dependencies]
abi_trait = { git = "https://gitlab.com/zack-emmert/abi_trait.git"}
```

Add the `#[interface]` attribute to a trait you want to create an FFI-safe dynamic dispatch object for:
```rust,ignore
use abi_trait::interface;

#[interface]
trait Trait {
    fn foo(&self);
    fn bar(&mut self);
    fn baz(self) -> i32;
    fn qux(&self, x: u16);
    fn quux(&self, y: &mut u16);
}
```
If you would like a given trait to represent the root object of downstream plugins, simply pass `root` as a parameter
to `#[interface]`

```rust,ignore
use abi_trait::interface;
#[interface(root)]
trait Trait {
    fn foo(&self);
    fn bar(&mut self);
    fn baz(self) -> i32;
    fn qux(&self, x: u16);
    fn quux(&self, y: &mut u16);
}
```

The root object represents the pseudo-Trait Object that is loaded directly from the compiled plugin from the binary.
Any other objects are loaded by calling functions defined on the root.

In downstream plugins you can add the`#[root]` attribute on a function that returns the pseudo-Trait Object and takes no
parameters. This function is called as part of the loading process for the root object.

```rust,ignore
use abi_trait::root;
use interface_crate::{DynTrait,Trait};
struct Struct(u16);

impl Trait for Struct {
    fn foo(&self) {/* ... */}
    fn bar(&mut self) {/* ... */}
    fn baz(self) -> i32 {/* ... */}
    fn qux(&self, x: u16) {/* ... */}
    fn quux(&self, y: &mut u16) {/* ... */}
}

#[root]
fn load() -> DynTrait {
    DynTrait::new(Struct(0))
}
```
Additionally, the trait objects are equipped with the `downcast_ref` and `downcast_mut` methods, which attempt to
downcast the trait objects to their original type, returning an `Option<&T>` and `Option<&mut T>`, respectively.
These return `Some` if the type downcasted to is the same as the type it was created with, and the function is called
within the same binary or shared library whose code originally created the pseudo-trait object. If either of these
conditions are not met, the functions will return `None`.

See `INTERNALS.md` for the implementation details of these two functions.

Lastly, In order to properly load a plugin and execute functions defined within it, do as follows:

```rust,ignore
use interface_crate::{DynTrait,Trait};

fn main() -> Result<(),Box<dyn std::error::Error>> {

    // Note that this is a convenience function that works by explicitly leaking memory
    //  and that any other method of acquiring an &'static Library will serve its purpose identically
    let lib = abi_trait::load("/path/to/plugin.so")?;

    let mut obj = DynTrait::load(lib)?;

    obj.foo();
    obj.bar();
    println!("{}",obj.baz());
    obj.qux(2);
    /* etc... */
    Ok(())
}
```

**NOTE:** Generally speaking, identifiers, type parameters, and other items beginning with `__abi_trait_`
(case *in*sensitive) are reserved for internal use by the macros.

## Roadmap ##

- [x] Documentation/examples
- [x] Error messaging/failures for invalid traits
- [x] Support for generic traits
- [ ] Support for associated types
- [x] Simpler, safer API for binaries that load the traits
*/

pub use abi_trait_macro::{interface, root};
pub use libloading;
pub use safe_types::*;
pub mod err;
mod loading;
mod safe_types;
pub use loading::*;

/// This trait is only implemented by the root module in the plugin and allows it to be loaded from
/// the binary. *Binary crate and interface crate authors should not
/// implement this trait*.
/// ## Safety
/// This trait is automatically implemented by the `interface` macro on the root trait object.
/// DO NOT implement it yourself!
#[doc(hidden)]
pub unsafe trait Root {}

/// This marker trait asserts that the implementing type can safely be sent over an FFI boundary
/// without causing UB.
/// ## Safety
/// This type can only be safely implemented on `abi_trait`'s trait object structs, or on #[repr(C)]
/// types consisting only of other types that implement `FFISafe`.
/// Implementing this trait on an invalid type *WILL CAUSE UB* if it is passed over an FFI boundary.
pub unsafe trait FFISafe {}

// This function is only used to trigger the correct compiler error in the interface macro
// in the event that a type used as a parameter or return value does not implement FFISafe
#[doc(hidden)]
#[inline]
pub fn __check__<T: FFISafe>() {}

const API_VERSION: u64 = 0;
