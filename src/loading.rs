use crate::{err::LoadError, Root};
use libloading::{Library, Symbol};
use std::{ffi::OsStr, mem::MaybeUninit};

/// This function loads a shared lib, and returns a static reference by explicitly leaking memory.
/// Ideally, it should only be used for testing purposes. Production uses should rely on `once_cell`
/// or similar.
pub fn load_library(path: impl AsRef<OsStr>) -> Result<&'static Library, libloading::Error> {
    // These need to be loaded for the entire program, so it's OK to let OS to cleanup at the end
    Ok(Box::leak(Box::new(Library::new(path)?)))
}

#[doc(hidden)]
pub fn load_root_module<T: Root>(lib: &'static Library, magic_const: u128) -> Result<T, LoadError> {
    let mut p = MaybeUninit::uninit();

    unsafe {
        // Sanity check to make sure the lib was created with the right version of abi_trait
        let api_ver: Symbol<'_, *const u64> = lib.get(b"ABI_TRAIT_API_VER\0")?;
        // Verifies the interface crate
        let remote_magic_const: Symbol<'_, *const u128> = lib.get(b"ABI_TRAIT_MAGIC_CONST\0")?;
        if crate::API_VERSION != **api_ver {
            return Err(LoadError::AbiTraitVersionMismatch(**api_ver));
        } else if magic_const != **remote_magic_const {
            return Err(LoadError::InterfaceCrateMismatch);
        }
        let load: Symbol<'_, unsafe extern "C" fn(*mut T) -> ()> =
            lib.get(b"__abi_trait_get_root_module\0")?;
        load(p.as_mut_ptr());
        Ok(p.assume_init())
    }
}
