use std::fmt::{Display, Formatter};

/// Represents all possible caused by a failed plugin load.
#[derive(Debug)]
pub enum LoadError {
    AbiTraitVersionMismatch(u64),
    InterfaceCrateMismatch,
    InvalidLibError(libloading::Error),
}

impl Display for LoadError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        use LoadError::*;

        match self {
            AbiTraitVersionMismatch(ver) => write!(
                f,
                "Attempted to load library with an invalid abi_trait API version: {}. \
                 Correct version should be: {}",
                *ver,
                crate::API_VERSION
            ),
            InterfaceCrateMismatch => write!(
                f,
                "Attempted to load library with an invalid interface crate"
            ),
            InvalidLibError(e) => write!(f, "Error loading library: {}", e),
        }
    }
}

impl From<libloading::Error> for LoadError {
    fn from(e: libloading::Error) -> Self {
        Self::InvalidLibError(e)
    }
}

impl std::error::Error for LoadError {}
