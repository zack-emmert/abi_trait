// This module effectively constitutes a list of all std and primitive types that can be sent over FFI safely
use crate::FFISafe;
use std::ptr::NonNull;

unsafe impl FFISafe for i8 {}
unsafe impl FFISafe for i16 {}
unsafe impl FFISafe for i32 {}
unsafe impl FFISafe for i64 {}
unsafe impl FFISafe for i128 {}
unsafe impl FFISafe for isize {}

unsafe impl FFISafe for u8 {}
unsafe impl FFISafe for u16 {}
unsafe impl FFISafe for u32 {}
unsafe impl FFISafe for u64 {}
unsafe impl FFISafe for u128 {}
unsafe impl FFISafe for usize {}

unsafe impl FFISafe for bool {}
unsafe impl FFISafe for char {}
unsafe impl FFISafe for f32 {}
unsafe impl FFISafe for f64 {}

unsafe impl<T: FFISafe> FFISafe for *const T {}
unsafe impl<T: FFISafe> FFISafe for *mut T {}
unsafe impl<T: FFISafe> FFISafe for &T {}
unsafe impl<T: FFISafe> FFISafe for &mut T {}
unsafe impl<T: FFISafe> FFISafe for NonNull<T> {}
unsafe impl<T: FFISafe> FFISafe for Option<T> {}

#[cfg(feature = "async")]
mod async_support {
    use async_ffi::{LocalBorrowingFfiFuture,BorrowingFfiFuture,FfiContext,FfiPoll};
    use crate::FFISafe;

    unsafe impl<'a, T: FFISafe> FFISafe for LocalBorrowingFfiFuture<'a, T> {}
    unsafe impl<'a, T: FFISafe> FFISafe for BorrowingFfiFuture<'a,T> {}
    unsafe impl<'a> FFISafe for FfiContext<'a> {}
    unsafe impl<T: FFISafe> FFISafe for FfiPoll<T> {}
}